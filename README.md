COOKiES Google Tag Manager Consent
----------------------------------

Provides cookies integration with the new Google Tag Manager consent mode.

This module adds third party settings to the Cookies Service entity form to add
control over GTM consent settings.
